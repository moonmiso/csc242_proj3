/*
 * Created by: Anesi Ikhianosime, Muskaan Mendiratta
 * Created on: March 31 2019
 * File name: LikelihoodWeightingInferencer.java 
 */
package bn.inference;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.sun.xml.internal.ws.api.pipe.NextAction;

import bn.core.*;

public class PriorSampler {
	protected BayesianNetwork network;
	protected Random random;
	protected List<RandomVariable> variables;
	
	PriorSampler(BayesianNetwork network){
		this.network = network;
	}
	PriorSampler(BayesianNetwork network, long seed){
		this.network = network;
		random = new Random(seed);
	}
	
	public Assignment getSample() {
		Assignment a = new bn.base.Assignment();
		for (RandomVariable X : network.getVariables()) {
			a.put(X, randomSampleForVariable(X, a));
		}
		return a;
	}
	
	protected Value randomSampleForVariable(RandomVariable Xi, Assignment x) {
		double random = Math.random();
		double threshold= 0.0;
		Assignment evidence = x.copy();

		for (Value v : Xi.getDomain()) {
			evidence.put(Xi, v);
			threshold+=network.getProbability(Xi, evidence);
			if (random <= threshold) {
				return v;
			}
		}
	
		return null;
	}

}

/*
 * Created by: Anesi Ikhianosime, Muskaan Mendiratta
 * Created on: March 31 2019
 * File name: LikelihoodWeightingInferencer.java 
 */

package bn.inference;

import java.util.ArrayList;

import bn.core.Assignment;
import bn.core.BayesianNetwork;
import bn.core.RandomVariable;
import bn.core.Value;

public class WeightedSampler extends PriorSampler{

	WeightedSampler(BayesianNetwork network) {
		super(network);
		this.network = network;
	}
	WeightedSampler(BayesianNetwork network, long seed){
		super(network, seed);
	}

	public ArrayList<Object> weightedSample (Assignment e){
		double w = 1;
		Assignment x = e.copy();

		ArrayList<Object> returner = new ArrayList<>();
		for (RandomVariable X : network.getVariablesSortedTopologically()) {
			Value value = x.get(X);
			if (value != null) {
				w = w * network.getProbability(X, x);
			}else {
				x.put(X, randomSampleForVariable(X, x));
			}
		}
		returner.add(w);
		returner.add(x);
		return returner;
	}


}

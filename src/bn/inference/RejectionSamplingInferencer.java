/*
 * Created by: Anesi Ikhianosime, Muskaan Mendiratta
 * Created on: March 31 2019
 * File name: LikelihoodWeightingInferencer.java 
 */
package bn.inference;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import bn.core.*;
import bn.parser.BIFParser;
import bn.parser.XMLBIFParser;

public class RejectionSamplingInferencer implements Inferencer{

	@Override
	public Distribution query(RandomVariable X, Assignment e, BayesianNetwork network) {
		System.out.println("Rejection Sampler");
		return query(X, e, network, 1000000);
	}

	public Distribution query(RandomVariable X, Assignment e, BayesianNetwork network, int n) {
		bn.base.Distribution N = new bn.base.Distribution(X); 
		for (Value value : X.getDomain()) {
			N.put(value, 0.0);
		}
		for (int j = 0; j < n; j++) {
			PriorSampler sampler = new PriorSampler(network);
			Assignment x = sampler.getSample();
			if (isConsistent(x, e)) {
				Value value = x.get(X);
				N.put(value, (N.get(value) + 1.0)); 
			}
		}
		N.normalize();
		return N;
	}

	public boolean isConsistent(Assignment x, Assignment e) {
		for (RandomVariable v1 : e.keySet()) {
			if (!(e.get(v1).equals(x.get(v1)))) {
				return false;
			}
		}
		return true;
	}

	public static void main (String[] args) throws NumberFormatException, 
	IOException,
	org.xml.sax.SAXException, 
	javax.xml.parsers.ParserConfigurationException{

		int N = Integer.parseInt(args[0]);
		String filePath = "./src/bn/examples/"+args[1];

		BayesianNetwork network ;
		if(filePath.endsWith(".xml")){


			network = new XMLBIFParser().readNetworkFromFile(filePath);

		} else if(filePath.endsWith(".bif")){

			network = new BIFParser(new FileInputStream(filePath)).parseNetwork();

		} else {
			System.out.println("Error: Invalid file input");
			return;
		}

		RandomVariable testVariable = network.getVariableByName(args[2].toString());
		Assignment assignment = new bn.base.Assignment();

		StringBuilder formula = new StringBuilder("P( " + args[1] + " | ");

		for(int i=3; i<args.length; i+=2) {
			Value b;
			if(args[i+1].equals("true")) {
				b= new bn.base.StringValue("true");
				assignment.put(network.getVariableByName(args[i]),b);
			}else if (args[i+1].equals("false")) {
				b= new bn.base.StringValue("false");
				assignment.put(network.getVariableByName(args[i]),b);
			}

			formula.append(args[i]).append("=").append(args[i + 1]).append(" ");
		}
		formula.append(")");

		System.out.println("Rejection Sampling Inferencer: ");

		RejectionSamplingInferencer e = new RejectionSamplingInferencer();
		System.out.println(formula+"\n"+args[1]+": "
				+ e.query(testVariable, assignment, network));



	}

}

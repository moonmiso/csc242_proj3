/*
 * File: EnumerationInferencer.java
 * Creator: Anesi Ikhianosime, Muskaan Mendiratta
 * Created: Thur March 21 2019
 */

package bn.inference;
import bn.core.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;


public class EnumerationInferencer implements Inferencer{

	public double enumerateAll(List<RandomVariable> vars, Assignment e, BayesianNetwork network) {
		if (vars.isEmpty()) {
			return 1.0;
		}
		RandomVariable Y = vars.get(0);
		Value value = e.get(Y);
		if (value != null) {
			//return P(y | parents(Y)) x ENUMERATE-ALL(REST (VARS), e)
			Assignment evidence = e.copy();
			return network.getProbability(Y, evidence) * enumerateAll(rest(vars), evidence, network);
		}else {
			e.put(Y, value);
			double sum = 0;
			Assignment evidence = e.copy();
			for (Value value2 : Y.getDomain()) {
				evidence.put(Y, value2);
				//return sum of y, P(y | parents(Y)) x ENUMERATE-ALL(REST(vars), ey)
				sum = sum + network.getProbability(Y, evidence)*enumerateAll(rest(vars), evidence, network);
			}
			return sum;
		}
	}
	public List<RandomVariable> rest(List<RandomVariable> vars){
		List<RandomVariable> rest = new ArrayList<>();
		for (int i = 1; i < vars.size(); i++) {
			rest.add(vars.get(i));
		}
		return rest;
	}
	public Distribution enumerateAsk(RandomVariable X, Assignment e, BayesianNetwork network) {
		bn.base.Distribution Q = new bn.base.Distribution(X); 
		network.getVariablesSortedTopologically();
		for (Value x : X.getDomain()) {
			Assignment evidence = e.copy();
			evidence.put(X, x);
			List<RandomVariable> list = new ArrayList<>(network.getVariables());
			Q.set(x, enumerateAll(list, evidence, network)); 
		}
		Q.normalize();
		return Q;
	}

	@Override
	public bn.core.Distribution query(RandomVariable X, Assignment e, bn.core.BayesianNetwork network) {
		return enumerateAsk(X,e,network);
	}
	
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException{		  

	      String filePath = "./src/bn/examples/"+args[0];

	      BayesianNetwork network ;
	      if(filePath.endsWith(".xml")){


	              network = new bn.parser.XMLBIFParser().readNetworkFromFile(filePath);

	      } else if(filePath.endsWith(".bif")){

	              network = new bn.parser.BIFParser(new FileInputStream(filePath)).parseNetwork();

	      } else {
	          System.out.println("Error: Invalid file input");
	          return;
	      }

	      RandomVariable testVariable = network.getVariableByName(args[1].toString());
	      Assignment assignment = new bn.base.Assignment();

	      StringBuilder formula = new StringBuilder("P( " + args[1] + " | ");

	      for(int i=2; i<args.length; i+=2) {
	    	  Value b;
	    	  if(args[i+1].equals("true")) {
	    		 b= new bn.base.StringValue("true");
	    		 assignment.put(network.getVariableByName(args[i]),b);
	    	  }else if (args[i+1].equals("false")) {
	    		  b= new bn.base.StringValue("false");
	     		 assignment.put(network.getVariableByName(args[i]),b);
	    	  }

	          formula.append(args[i]).append("=").append(args[i + 1]).append(" ");
	      }
	      formula.append(")");

	      System.out.println("Enumeration Inferencer: ");
	      
	      EnumerationInferencer e = new EnumerationInferencer();
	      System.out.println(formula+"\n"+args[1]+": "
	              + e.query(testVariable, assignment, network));
	}


}

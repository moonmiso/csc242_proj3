/*
 * Created by: Anesi Ikhianosime, Muskaan Mendiratta
 * Created on: March 31 2019
 * File name: LikelihoodWeightingInferencer.java 
 */
package bn.inference;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import bn.base.BooleanDomain;
import bn.base.BooleanValue;
import bn.base.NamedVariable;
import bn.core.*;
import bn.parser.BIFParser;
import bn.parser.XMLBIFParser;
import bn.parser.XMLBIFPrinter;

public class LikelihoodWeightingInferencer implements Inferencer{

	@Override
	public Distribution query(RandomVariable X, Assignment e, BayesianNetwork network) {
		return query(X, e, network, 100000);
	}

	public Distribution query (RandomVariable X, Assignment e, BayesianNetwork network, int n) {
		Distribution vector = new bn.base.Distribution(X);
		WeightedSampler sampler = new WeightedSampler(network);
		for (Value value : X.getDomain()) {
			vector.put(value, 0.0);
		}
		for (int j = 1; j <= n; j++) {
			List <Object> sample = sampler.weightedSample(e);
			//x, w -> weighted
			Double w = (Double) sample.get(0);
			Assignment x = (Assignment) sample.get(1);
			Value value = x.get(X);
			vector.put(value, (vector.get(value) + w));
		}		
		vector.normalize();
		return vector;
	}
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException{

		int N = Integer.parseInt(args[0]);
		String filePath = "./src/bn/examples/"+args[1];

		BayesianNetwork network ;
		if(filePath.endsWith(".xml")){


			network = new XMLBIFParser().readNetworkFromFile(filePath);

		} else if(filePath.endsWith(".bif")){

			network = new BIFParser(new FileInputStream(filePath)).parseNetwork();

		} else {
			System.out.println("Error: Invalid file input");
			return;
		}

		RandomVariable testVariable = network.getVariableByName(args[2].toString());
		Assignment assignment = new bn.base.Assignment();

		StringBuilder formula = new StringBuilder("P( " + args[1] + " | ");

		for(int i=3; i<args.length; i+=2) {
			Value b;
			if(args[i+1].equals("true")) {
				b= new bn.base.StringValue("true");
				assignment.put(network.getVariableByName(args[i]),b);
			}else if (args[i+1].equals("false")) {
				b= new bn.base.StringValue("false");
				assignment.put(network.getVariableByName(args[i]),b);
			}

			formula.append(args[i]).append("=").append(args[i + 1]).append(" ");
		}
		formula.append(")");

		System.out.println("Likelihood Weighting Inferencer: ");

		LikelihoodWeightingInferencer e = new LikelihoodWeightingInferencer();
		System.out.println(formula+"\n"+args[1]+": "
				+ e.query(testVariable, assignment, network));

	}
}

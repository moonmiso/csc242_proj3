CSC242_Proj2
Name: Anesi Ikhianosime, Muskaan Mendiratta
Assignment Number: Project 03, Unit 03
We did not collaborate with anyone else on this project.

Algorithms:
EnumerationInferencer.java
RejectionSamplingInferencer.java
LikelihoodWeightingInferencer.java


Compiling and executing instructions:
This project has been written in Java.
Steps for compiling and executing:
javac *.java
java EnumerationInferencer.java fileName QVar [EVar Value] 
java RejectionSamplingInferencer N fileName QVar [EVar Value]
java LikelihoodWeightingInferencer N fileName QVar [EVar Value]

OR
Go to Eclipse, and "run configurations" 
for EnumerationInferencer.java, aruments are: fileName QVar [EVar Value] 
eg: aima-alarm.xml B J true M true
for RejectionSamplingInferencer, arguments are: N fileName QVar [EVar Value]
eg: 10000 aima-alarm.xml B J true M true
for LikelihoodWeightingInferencer, arguments are: N fileName QVar [EVar Value]
eg: 10000 aima-alarm.xml B J true M true


On the console (outputs):
Enumeration Inferencer: 
P( B | J=true M=true )
B: {true=0.2841718353643929, false=0.7158281646356071}

Rejection Sampling Inferencer: 
Rejection Sampler
P( aima-alarm.xml | J=true M=true )
aima-alarm.xml: {true=0.28477133427628476, false=0.7152286657237152}

Likelihood Weighting Inferencer: 
P( aima-alarm.xml | J=true M=true )
aima-alarm.xml: {true=0.268457530097276, false=0.731542469902724}




